import Button from './components/Button';
import Modal from './components/Modal';
import Parent from './components/Parent'
import React from 'react'

class App extends React.Component {
  constructor() {
    super();

  }

  render() {
  

    return(
      <div>
        <Button/>
        <Modal/>
      </div>
    )
  }
  
}

export default App;
