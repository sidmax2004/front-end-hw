import Button from "./components/Button";
import React from 'react';
import Modal from "./components/Modal";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
        this.onDialogShow = this.onDialogShow.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
    }

    onDialogShow() {
        this.setState({
            show: true
        })
    }

    onDialogClose(obj) {
        console.log('Data from child -> ', obj);

        this.setState({
            show: false
        })
    }

    render() {
        const args = {
            title:'jgjhjy'
        };

        return (
            <div className='application-wrapper'>
                <Button onClick={this.onDialogShow} title='click me'/>
                <Modal title='Test modal'
                       showCloseButton={true}
                       show={this.state.show}
                       onClose={this.onDialogClose}
                       // onClose={() => {
                       //     alert('Hello!!!');
                       // }}

                >
                    <h1>Hello hello</h1>
                    <h3>Hello hello</h3>
                    <h5>Hello hello</h5>
                </Modal>
            </div>
        );
    }
}

export default App;
