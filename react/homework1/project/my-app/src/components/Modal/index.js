import React from 'react';
import './Modal.css';
import PropTypes from 'prop-types';
import Button from "../Button";

class Modal extends React.Component{
    constructor(props) {
        super(props);

        this.onClose = this.onClose.bind(this);
    }

    onClose(){
        this.props.onClose({someDate: 'quick message'});
    }

    render(){
        const styles = {
            display: this.props.show ? 'block' : 'none'
        }

        return(
            <div style={styles} className='modal'>
                <div className='modal-header'>
                    {
                        this.props.showCloseButton
                            ? (<span className='close-dialog'
                                     onClick={this.onClose}>x</span>)
                            : null
                    }
                    {this.props.title}
                </div>
                <div className='modal-body'>
                    {this.props.children}
                </div>
                <div className='modal-footer'>
                    <Button title='close' onClick={this.onClose}/>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    showCloseButton: PropTypes.bool,
    title: PropTypes.string,
    show: PropTypes.bool,
    onClose: PropTypes.func
}

export default Modal