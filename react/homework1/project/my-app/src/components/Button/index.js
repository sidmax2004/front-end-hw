import React from 'react';
import PropTypes from 'prop-types';


class Buttom extends React.Component {
    constructor() {
        super(props);

        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick() {
        this.props.onClick();
    }

    render() {
        return (
            <button onClick={this.onBtnClick}>{this.props.title}</button>
        );

    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    title: PropTypes.string
}

export default Button;