import './App.css';
import React from 'react'
import PersonList from './components/GetItems'

class App extends React.Component {
  
  render() {
    return (
      <div className="App">
        <PersonList/>
      </div>
    );
  }
  
}

export default App;
