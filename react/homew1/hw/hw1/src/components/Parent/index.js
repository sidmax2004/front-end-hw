import React from 'react';
import PropTypes from 'prop-types'


class Parent extends React.Component {
    constructor(props, bgcolor, text) {
        super(props)
        this.bgcolor = bgcolor;
        this.text = text;

        this.onClick=this.onClick.bind(this)

    }


    onClick() {
        this.props.onClick();
    }

    
}

Parent.propTypes = {
    onClick: PropTypes.func
}

export default Parent;