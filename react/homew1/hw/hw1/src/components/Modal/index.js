import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";
import './Modal.css'
import Parent from '../Parent/index'


export default class Modal extends Parent {
    constructor(props) {
        super(props);


        this.onClose = this.onClose.bind(this)
    }

    onClose() {
        this.props.onClose()
    }


    render() {
        const styles = {
            display: this.props.show ? 'block' : 'none'
        }
        return (
            <div style={styles} className='modal-wrapper'>
                <h1 className='modal-title'>{this.text = ' Do you want to delete this file?'}</h1>
                <button className='modal-button' onClick={this.onClose}>{this.text = 'CLOSE'}</button>
                <div className='modal-span'>
                    <p className='modal-text'>{this.text ='Once you delete this file, it won’t be possible to undo this action'}</p>
                    <p className='modal-text'>{this.text = 'Are you sure you want to delete it?'}</p>
                    
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    onClose: PropTypes.func
}
