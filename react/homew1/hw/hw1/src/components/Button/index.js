import React from 'react';


import Parent from '../Parent/index.js'

import PropTypes from 'prop-types'


class Button extends Parent {
    constructor(props) {
        super(props);

        this.text = 'First Button'
        this.bgcolor = 'red'

    }



    render() {
        return(
           <div>
               <button onClick color={this.bgcolor}>{this.text = 'First Button'}</button>
               <button>{this.text = 'Second Button'}</button>
           </div>
           
        )
    }
}

export default Button;