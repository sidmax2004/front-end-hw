import Button from './components/Button';
import Modal from './components/Modal';
import Parent from './components/Parent'
import React from 'react'

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      show: false
  }
  this.onDialogShow = this.onDialogShow.bind(this);
  this.onDialogClose = this.onDialogClose.bind(this);



  }
  onDialogShow() {
    this.setState({
        show: true
    })
}

onDialogClose(obj) {
    console.log('Data from child -> ', obj);

    this.setState({
        show: false
    })
}




  render() {
  

    return(
      <div>
        <Button onClick={this.onDialogShow}/>
        <Modal
        showCloseButton={true}
        show={this.state.show}
        onClose={this.onDialogClose}

        />
      </div>
    )
  }
  
}

export default App;
