import Button from "./components/Button";
import React from 'react';
import Modal from "./components/Modal";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
        this.onDialogShow = this.onDialogShow.bind(this);
        this.onDialogClose = this.onDialogClose.bind(this);
    }

    onDialogShow() {
        this.setState({
            show: true
        })
    }

    onDialogClose(obj) {
        console.log('Data from child -> ', obj);

        this.setState({
            show: false
        })
    }

    render() {
        const args = {
            title:'jgjhjy'
        };

        return (
            <div className='application-wrapper'>
                <Button onClick={this.onDialogShow} title='First Button'/>
                <Button onClick={this.onDialogShow} title='Second Button'/>
                <Modal title='LOGIN'
                       show={this.state.show}
                       onClose={this.onDialogClose}

                >
                    <h1>HELLO</h1>
                   <input></input>
                </Modal>
            </div>
        );
    }
}

export default App;
