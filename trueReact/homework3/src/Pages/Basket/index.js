import React, { useState, useEffect } from 'react'
import CardList from "../../components/CardList";
import phonesListJSON from '../../services/index'
import {basketPhonesLocalSt} from "../../services/index-json";


export default function Basket (props) {

    const someArr = []
    async function arrayJson() {
        const {data} = await phonesListJSON()
        data.map(item => {
            someArr.push(item)
        })
    }
    arrayJson()

    const [basket, setBasket] = useState([])

    useEffect(() => {
        async function jsonPhonesArray() {
            const {data} = await phonesListJSON();
            const arrayLocalGet = basketPhonesLocalSt();
            const newArray = []
            data.forEach(item => {
                if (arrayLocalGet.includes(item.vendor_code)) {
                    newArray.push(item)
                }
            })
            setBasket(newArray);
        }
        jsonPhonesArray();
    }, [])

    const deleteCardFromBasket = () => {
        const arrayLocalGet = basketPhonesLocalSt();
        const newArray = []
        someArr.forEach(item => {
            if (arrayLocalGet.includes(item.vendor_code)) {
                newArray.push(item)
            }
        })
        setBasket(newArray)
    }

    return (
        <div>
            <CardList
                setPhonesBasket={props.setPhonesBasket}
                phones={basket}
                baskets2={basket}
                deleteCardFromBasket={deleteCardFromBasket}
            />
        </div>
    )
}
