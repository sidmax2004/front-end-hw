import React from 'react'
import Card from "../Card";


function CardList(props) {
    return (
        <div className='card-group'>
            {props.phones.map(phone =>
            <Card phone={phone}
                  key={phone.vendor_code}
                  deleteCardFromBasket={props.deleteCardFromBasket}
                  deleteFromFavoritesPage={props.deleteFromFavoritesPage}
                  baskets2={props.baskets2}
                  favorites2={props.favorites2}
                  setPhonesBasket={props.setPhonesBasket}
                  setPhonesFavorite={props.setPhonesFavorite}
            />
            )}
        </div>
    )
}

export default CardList