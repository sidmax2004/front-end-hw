import React from 'react'
import Home from "../../Pages/Home";
import Favorites from "../../Pages/Favorires";
import Basket from "../../Pages/Basket";
import {Route, Switch} from "react-router-dom";

function Main(props) {
    return (
        <Switch>
            <Route exact path='/'>
                <Home
                    setPhonesBasket={props.setPhonesBasket}
                    setPhonesFavorite={props.setPhonesFavorite}
                />
            </Route>
            <Route path='/baskets'>
                <Basket
                    setPhonesBasket={props.setPhonesBasket}
                    setPhonesFavorite={props.setPhonesFavorite}
                />
            </Route>
            <Route path='/favorites'>
                <Favorites
                    setPhonesBasket={props.setPhonesBasket}
                    setPhonesFavorite={props.setPhonesFavorite}
                />
            </Route>
        </Switch>
    )
}

export default Main