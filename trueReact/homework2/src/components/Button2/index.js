import React from 'react'
import './Button2.scss'

class Button2 extends React.Component{
    constructor(props) {
        super(props);
        this.onBtnClick = this.onBtnClick.bind(this)
    }

    onBtnClick () {
        console.log('BTN CLICK')
    }

    render() {
        return (
            <>
                <button className={this.props.btnClassName} onClick={this.props.onClick}>{this.props.btnText}</button>
            </>
        );
    }
}

export default Button2