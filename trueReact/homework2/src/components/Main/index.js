import React from 'react'
import {Switch, Route} from "react-router-dom";
import PhoneList2 from "../../Pages/AllCards2";
import Favorites2 from "../../Pages/Favorites2";
import BasketList2 from "../../Pages/Basket2";


class Main extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <Switch>
                <Route exact path='/' component={PhoneList2}/>
                <Route path='/favorites' component={Favorites2}/>
                <Route exact path='/baskets' component={BasketList2}/>
            </Switch>
        );
    }
}

export default Main