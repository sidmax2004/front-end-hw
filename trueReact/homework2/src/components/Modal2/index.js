import React from 'react'
import './Modal2.scss'

class Modal2 extends React.Component {
    constructor(props) {
        super(props);
        this.closeModalOnClick = this.closeModalOnClick.bind(this)
    }
    closeModalOnClick () {
        this.props.closeModal()
    }

    render() {
        const {namePhone,imageUrl} = this.props.card
        return (
            <div className='modal' onClick={this.closeModalOnClick}>
                <div className='modal-body' onClick={e => e.stopPropagation()} >
                    <div className='modalHeader'>
                        <p> Телефон
                            <span className='modal-card-span'> {namePhone}</span> добавлен в корзину
                            <span className='closeCross' onClick={this.closeModalOnClick}>&times;</span>
                        </p>
                        <img className='modal-image' src={imageUrl} alt=""/>
                    </div>
                </div>
            </div>
        );
    }


}

export default Modal2