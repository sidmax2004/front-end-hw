import React from 'react'
import axios from 'axios'
import PhoneCard2 from "../../components/PhoneCard2";

class Basket2 extends React.Component {
    constructor(props) {
        super(props);
        this.deleteFromBasket = this.deleteFromBasket.bind(this)
    }
    state = {
        cards: [],
    }

    async componentDidMount() {
        const {data} = await axios.get('http://localhost:3000/phones.json')
        this.setState({cards: data});
        const arrayFromLocalStorage = JSON.parse(localStorage.getItem('card'))
        if (arrayFromLocalStorage === null || arrayFromLocalStorage.length === 0) {
            this.setState({cards: []})
        }
        if (arrayFromLocalStorage !== null) {
            const newArray = []
            data.forEach(card => {
                if (arrayFromLocalStorage.includes(card.vendor_code)) {
                    newArray.push(card)
                    this.setState({cards: newArray});
                }
            })
        }
    }

    async deleteFromBasket () {
        const {data} = await axios.get('http://localhost:3000/phones.json')
        const firstStateCards = this.state.cards
        console.log('firstStateCards',firstStateCards)
        const arrayFromLocalStorage = JSON.parse(localStorage.getItem('card'))
        const newArray = []
        data.forEach(card => {
            if (arrayFromLocalStorage.includes(card.vendor_code)) {
                newArray.push(card)
                this.setState({cards: newArray});
            }
            if (arrayFromLocalStorage.length === 0 || arrayFromLocalStorage.length === null) {
                this.setState({cards: []})
            }
            else {
                console.log('gecnj')
            }
        })
    }



    render() {
        return (
                <div className="card-wrapper">
                    {this.state.cards.map(card =>
                            <PhoneCard2
                                card={card}
                                // key={card.vendor_code}
                                btn={true}
                                deleteBasket={true}
                                deleteFromBasket={this.deleteFromBasket}
                                btnBasket={true}
                                addToFavorite={true}
                            />
                    )}
                </div>
        );
    }
}

export default Basket2