import React from 'react'
import axios from 'axios'
import PhoneCard2 from "../../components/PhoneCard2";


class PhoneList2 extends React.Component {
    constructor(props) {
        super(props);
    }
    state = {
        cards: []
    }

    async componentDidMount() {
        const {data} = await axios.get('http://localhost:3000/phones.json')
        this.setState({cards: data});
    }


    render() {
        return (
                <div className="card-wrapper">
                    {this.state.cards.map(card =>
                        <PhoneCard2 card = {card}
                                    // key={card.vendor_code}
                                    addToFavorite={true}
                                    btnBasket={true}
                                    showCard={true}
                        />
                    )}
                </div>
        );
    }
}

export default PhoneList2