import React from 'react'
import axios from 'axios'
import PhoneCard2 from "../../components/PhoneCard2";


class Favorites2 extends React.Component {
    constructor(props) {
        super(props);
        this.deleteFromFavor = this.deleteFromFavor.bind(this)
    }

    state = {
        cards: [],
    }

    async componentDidMount() {
        const {data} = await axios.get('http://localhost:3000/phones.json')
        this.setState({cards: data});
        const arrayFromLocalStorage = JSON.parse(localStorage.getItem('favorite'))
        if (arrayFromLocalStorage === null || arrayFromLocalStorage.length === 0) {
            this.setState({cards: []});
        }
        if (arrayFromLocalStorage !== null) {
            const newData = []
            data.forEach(card => {
                if (arrayFromLocalStorage.includes(card.vendor_code)) {
                    newData.push(card)
                    this.setState({cards: newData});
                }
            })
        }
    }

    async deleteFromFavor () {
        const {data} = await axios.get('http://localhost:3000/phones.json')
        const arrayFromLocalStorage = JSON.parse(localStorage.getItem('favorite'))
        if ( arrayFromLocalStorage === null || arrayFromLocalStorage.length === 0) {
            this.setState({cards: []})
        }
        if (arrayFromLocalStorage !== null) {
            const newArray = []
            data.forEach(card => {
                if (arrayFromLocalStorage.includes(card.vendor_code)) {
                    newArray.push(card)
                    this.setState({cards: newArray});
                }
            })
        }
    }

    render() {
        return (
                <div className="card-wrapper">
                    {
                        this.state.cards.map(card =>
                        <>
                            <PhoneCard2 card = {card}
                                        key={card.vendor_code}
                                        btnBasket={true}
                                        deleteFavorite={true}
                                        addToFavorite={false}
                                        deleteFrom={this.deleteFromFavor}
                            />
                        </>
                    )}
                </div>
        );
    }
}

export default Favorites2
