import React from 'react'
import './App.css';
import {Link} from "react-router-dom";
import Main from "./components/Main";

class App extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="App">
                <nav className='nav-menu'>
                    <ul className='nav-menu-ul'>
                        <li>
                            <Link className='app-links' to={`/`}>AllPhones</Link>
                        </li>
                        <li>
                            <Link to={`/favorites/`}>Favorites</Link>
                        </li>
                        <li>
                            <Link to={`/baskets/`}>BasketList</Link>
                        </li>
                    </ul>
                </nav>
                <Main/>
            </div>
        )
    }
}

export default App;
