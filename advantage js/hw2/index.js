
try {
    const div = document.querySelector('#root');
    const books = [
        {
            author: "Скотт Бэккер",
            name: "Тьма, что приходит прежде",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Воин-пророк",
        },
        {
            name: "Тысячекратная мысль",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Нечестивый Консульт",
            price: 70
        },
        {
            author: "Дарья Донцова",
            name: "Детектив на диете",
            price: 40
        },
        {
            author: "Дарья Донцова",
            name: "Дед Снегур и Морозочка",
        }
    ];
     books.forEach(function (book, i) {
         const li = document.createElement('li')
         li.innerHTML = `${book.author} || ${book.name} || ${book.price}`
         div.appendChild(li)

         if (!book.author) {
             div.removeChild(li)
         }
         if (!book.price) {
             div.removeChild(li)
         }
    })





} catch (e) {

}










