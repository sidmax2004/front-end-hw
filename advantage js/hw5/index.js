const btnIp = document.querySelector('.btn-ip')

btnIp.addEventListener('click', getIp)


async function getIp(options = {},) {
    const getIpItem = await fetch('https://api.ipify.org/?format=json', {
        method: 'GET'
    })
    const ipData = await getIpItem.json()
    console.log(ipData.ip)

    const res2 = await fetch(`http://ip-api.com/json/${ipData.ip}`, {
        ...options
    })
    const dataItem = await res2.json()


    PostLocation(dataItem)
    console.log(dataItem)

}


function PostLocation(data) {
    const div = document.querySelector('#root')
    div.innerHTML = JSON.stringify(data)
}














